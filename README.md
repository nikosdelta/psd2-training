# PSD2 Training

- Sign in στην Εθνική μέσω του `/authorize` endpoint. Θα σε κάνει redirect κατευθείαν στην Εθνική. Μόλις δώσεις τα δικαιώματα, το redirect_uri θα σε πάει στο `/token` endpoint όπου εκεί θα αποθηκευτεί στο map το refresh token και θα πάρει και ο χρήστης ένα `user_id` για να προσομοιώσουμε λίγο το authentication.
- Μέσω του `/accounts` αφού βάλεις ως query param `user_id={USER_ID}`, τότε θα πάρεις πίσω την fake json απάντηση. Αλλιώς θα πάρεις 401 και το URI για να κάνεις το authorization.