import express from 'express';
import { v4 as uuidv4 } from 'uuid';

const app = express();
require('dotenv').config()
const port = 3000;
const request = require("request");

//Simulate db
const tokens = new Map();

const requireBankAuthentication = function (req: any, res: any, next: any) {
        const user_id = req.query.user_id as string
        if (user_id === undefined) {
            const base_url = req.protocol + '://' + req.get('host') + '/authorize'
            res.status(401).json({status: 'Unauthenticated. Please authenticate using ' + base_url}).send() //Simulate unauthenticated
            return;
        } else{
            req.headers.authorization = getAccessToken(user_id);
        }
        next();
}

function getAccessToken(user_id: string){

    const refresh_token = JSON.parse(tokens.get(user_id)).refresh_token as string;

    const options = { method: 'POST',
        url: 'https://my.nbg.gr/identity/connect/token',
        headers:
            { 'cache-control': 'no-cache',
                'content-type': 'application/x-www-form-urlencoded',
                'accept': 'application/json' },
        form:
            {   client_id: 'D4718478-27D8-4F20-8C9C-65F515E27B97',
                client_secret: '6416187D-0E0D-4CD1-BA87-CA0D7E5A4489',
                grant_type: 'refresh_token',
                refresh_token: refresh_token
            } };

    request(options, function (error: string | undefined, response: any, body: any) {
        if (error) throw new Error(error);
        tokens.set(user_id,body)
        console.log(body)
        return body['access_token'];
    });

    return "test";
}

app.get('/authorize', (req, res) => {
    res.status(200).redirect(`https://my.nbg.gr/identity/connect/authorize?response_type=code&client_id=${process.env.CLIENT_ID}&scope=sandbox-bg-ob-accounts+offline_access&redirect_uri=${process.env.REDIRECT_URI}`);
});

app.get('/token', (req, res) => {

    const user_id = uuidv4();

    const options = { method: 'POST',
        url: 'https://my.nbg.gr/identity/connect/token',
        headers: {
                'cache-control': 'no-cache',
                'content-type': 'application/x-www-form-urlencoded',
                'accept': 'application/json'
        },
        form: {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: 'authorization_code',
                code: req.query.code,
                redirect_uri: process.env.REDIRECT_URI
            }
    };

    request(options, function (error: string | undefined, response: any, body: any) {
        if (error) throw new Error(error);
        console.log(body)
        tokens.set(user_id,body)
    });

    res.json({user_id: user_id})
});

app.get('/accounts', requireBankAuthentication, (req, res) => {
    res.json({
        "accounts": [
            {
                "resourceId": "9d49d3c0-6c92-4a61-4457-f0cea5a654a8",
                "iban": "GR3201106970000069774934603",
                "currency": "EUR",
                "ownerName": "FIRST BENEFICIARY",
                "displayName": "Account Alias",
                "product": "This is a personal savings account",
                "cashAccountType": "SVGS",
                "status": "enabled",
                "bic": "ETHNGRAA",
                "usage": "PRIV",
                "balances": [
                    {
                        "balanceAmount": {
                            "currency": "EUR",
                            "amount": "1120.67"
                        },
                        "balanceType": "interimAvailable"
                    },
                    {
                        "balanceAmount": {
                            "currency": "EUR",
                            "amount": "1150.67"
                        },
                        "balanceType": "interimBooked"
                    }
                ]
            }
        ]
    });
});

app.listen(port, () => {
    console.log(`Listening on port ` + port)
});

